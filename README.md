# The simplest force directed

A very simple force directed drawing method. Uses D3.js but only to draw graphic elements (it does not use the force method).

## Getting started

You can access a demo here: https://uniroma3.gitlab.io/compunet/gd/infovis-demos/the-simplest-force-directed/

## Todos

- Update the project with a more recent version of D3.js
- Allow the user to choose different kinds of default datasets
- Allow the user to save a dataset to the client hard disk

## Credits

This project was realized by Matteo Bellatreccia as part of his Bachelor degree final project.

